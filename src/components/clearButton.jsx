import React from 'react';
import PropTypes from 'prop-types';
import CloseIcon from './icons/closeIcon';
import '../styles/components/_clearBtn.scss';

const ClearButton = ({ clearBoard, disabled }) => (
  <button className="clearBtn"
    disabled={!disabled}
    onClick={() => clearBoard()}>
    <CloseIcon />
    Clear Word
  </button>
)

ClearButton.propTypes = {
  clearBoard: PropTypes.func,
  disabled: PropTypes.bool
};

export default ClearButton;
