import React from 'react';
import PropTypes from 'prop-types';
import '../styles/components/_letter.scss';

const LetterComponent = ({ letter, isSelected, index, handleSelectLetter }) => (
  <a className={`${isSelected && "active"} letter`}
    onClick={() => handleSelectLetter(index)}>
    {letter}
  </a>
)

LetterComponent.propTypes = {
  letter: PropTypes.string,
  isSelected: PropTypes.bool,
  index: PropTypes.number,
  handleSelectLetter: PropTypes.func
};

export default LetterComponent;
