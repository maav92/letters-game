import React from 'react';

const CloseIcon = () => (
  <svg width="20px" height="20px" viewBox="0 0 20 20">
    <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="Group" transform="translate(-3.000000, -2.000000)">
        <g id="baseline-add_circle-24px">
            <polygon id="Path" points="0 0 24 0 24 24 0 24"></polygon>
            <path d="M13,2 C7.48,2 3,6.48 3,12 C3,17.52 7.48,22 13,22 C18.52,22 23,17.52 23,12 C23,6.48 18.52,2 13,2 Z" id="Shape" fill="#D8D8D8" fillRule="nonzero"></path>
        </g>
        <g id="baseline-clear-24px" transform="translate(1.000000, 0.000000)">
            <polygon id="Path" fill="#FFFFFF" fillRule="nonzero" points="19 6.41 17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12"></polygon>
            <polygon id="Path" points="0 0 24 0 24 24 0 24"></polygon>
        </g>
      </g>
    </g>
  </svg>
)

export default CloseIcon;
