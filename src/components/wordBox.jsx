import React from 'react';
import PropTypes from 'prop-types';
import '../styles/components/_wordBox.scss'

const WordBox = ({ selectedLetters, isValidWord }) => (
  <div className={`wordBox ${isValidWord}`}>
    <span>{selectedLetters}</span>
    {
      selectedLetters.length
        ? <small>
          {isValidWord === 'validWord' ? 'valid' : 'invalid'}
        </small>
        : ''
    }
  </div>
)

WordBox.propTypes = {
  selectedLetters: PropTypes.string,
  isValidWord: PropTypes.string
}

export default WordBox;
