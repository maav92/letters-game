import React from 'react';
import PropTypes from 'prop-types';
import LetterComponent from './letter';
import '../styles/components/_board.scss';

const BoardComponent = ({ isValidWord, board, handleSelectLetter }) => (
  <div className={`lettersBoard ${isValidWord}`}>
    {
      board.map((item, index) => (
        <LetterComponent
          key={index}
          index={index}
          handleSelectLetter={handleSelectLetter}
          isSelected={item.isSelected}
          letter={item.letter}
        />
      ))
    }
  </div>
)

BoardComponent.propTypes = {
  isValidWord: PropTypes.string,
  board: PropTypes.array,
  handleSelectLetter: PropTypes.func
};

export default BoardComponent;
