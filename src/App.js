import React, { Component } from 'react';
import _ from 'lodash';
import { board } from './data/test-board-2.json';
import { words } from './data/dictionary.json';
import './styles/_index.scss';
import BoardComponent from './components/board.jsx';
import ClearButton from './components/clearButton.jsx';
import WordBox from './components/wordBox.jsx';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      boardLetters: [],
      selectedLetters: [],
      indexLetters: [],
      isValidWord: 'invalidWord'
    }
  }

  componentDidMount() {
    this.setInitialBoard();
  }

  // set the initial board 
  setInitialBoard = () => {
    const loadBoard = board.map(value => Object.assign({
      'letter': value,
      'isSelected': false
    }));
    this.setState({
      boardLetters: loadBoard,
      selectedLetters: [],
      indexLetters: []
    })
  }

  // add or remove letters from the formed word
  handleSelectLetter = index => {
    var word = '';
    var indexes = [...this.state.indexLetters];
    var board = [...this.state.boardLetters];
    const { letter, isSelected } = board[index];

    if (isSelected) {
      _.remove(indexes, function(e) { return e === index});
    } else {
      indexes = indexes.concat(index);
    }

    word = this.formedWord(indexes);
    board[index] = { letter, isSelected: !isSelected }

    this.setState({
      selectedLetters: word,
      isValidWord: this.checkWord(word),
      boardLetters: board,
      indexLetters: indexes
    });
  }

  // build the word formed
  formedWord = array => {
    var word = [];
    array.forEach(e => {
      word = word.concat(board[e]);
    });
    return word.join("");
  }

  // check if the word is in the dictionary
  checkWord = word => {
    var exist = 'invalidWord';
    words.forEach(e => {
      if(e === word.toLowerCase()) exist = 'validWord';
    });
    return exist;
  }

  render() {
    const { selectedLetters, isValidWord, boardLetters } = this.state;
    return (
      <div className="LettersGame">
        <h1>Letters Game!</h1>
        
        <div className="content">
          <ClearButton
            clearBoard={this.setInitialBoard}
            disabled={selectedLetters.length > 0}
          />
          <BoardComponent
            board={boardLetters}
            isValidWord={isValidWord}
            handleSelectLetter={this.handleSelectLetter}
            isSelected={this.isSelected}
          />
          <WordBox
            isValidWord={isValidWord}
            selectedLetters={selectedLetters}
          />
        </div>
      </div>
    );
  }
}

export default App;
